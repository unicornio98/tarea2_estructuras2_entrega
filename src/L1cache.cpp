/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <bits/stdc++.h>
#include "../include/L1cache.h"
#include "../include/debug_utilities.h"

#define KB 1024
#define ADDRSIZE 32
#define HIT true
#define MISS false
#define STORE true
#define LOAD false
#define INITIAL_VALUE_empty false
#define LRU 0
#define NEAR 0
#define DISTANT 1

using namespace std;

int field_size_get( int cachesize_kb,
                    int associativity,
                    int blocksize_bytes,
                    int *tag_size,
                    int *idx_size,
                    int *offset_size)
{
  /* Negative values banned */
  if(cachesize_kb <= 0 || associativity <= 0 || blocksize_bytes <= 0){
    return ERROR;
  }
  /* Are values power of 2? */
  if((ceil(log2(cachesize_kb*KB)) != floor(log2(cachesize_kb*KB))) ||
     (ceil(log2(associativity)) != floor(log2(associativity))) ||
     (ceil(log2(blocksize_bytes)) != floor(log2(blocksize_bytes)))){
       return ERROR;
     }
  /* Offset */
  *offset_size = log2(blocksize_bytes);
  /* Index */
  *idx_size = log2(cachesize_kb*KB)
            - log2(associativity)
            - (*offset_size);
  /* Tag */
  *tag_size = ADDRSIZE - (*idx_size) - (*offset_size);

  return OK;
}

void address_tag_idx_get(long address,
                        int tag_size,
                        int idx_size,
                        int offset_size,
                        int *idx,
                        int *tag)
{

  /* long to bit, displace, cast to int */

  /* Tag */
  bitset<ADDRSIZE>tagSize(address);
  tagSize >>= idx_size + offset_size;
  *tag = (int)(tagSize.to_ulong());
  /* Index */
  bitset<ADDRSIZE>idxSize(address);
  idxSize <<= tag_size;
  idxSize >>= tag_size + offset_size;
  *idx = (int)(idxSize.to_ulong());

}

int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
   if (associativity < 0 || tag < 0 || idx < 0)
   {
      return PARAM;
   }else if (cache_blocks == NULL)
   {
      return PARAM;
   }

   /* Define M according to associativity */
   int M;
   if (associativity <= 2)
   {
      M = 1;
      /* Same as NRU replacement policy */
   } else
   {
      M = 2;
   }

   int distant_rp_value = pow(2, M) - 1;
   int long_rp_value = pow(2, M) - 2;

   /* Define variables for next steps */
   int pos_to_replace = associativity;
   bool hit = false;

   /* Load case */
   if (!loadstore)
   {
      /* Cache hit */
      for (int i = 0; i < associativity; i++)
      {
         if (cache_blocks[i].tag == tag && cache_blocks[i].valid)
         {
            result->miss_hit = HIT_LOAD;
            result->dirty_eviction = false;

            /* Change rp_value */
            cache_blocks[i].rp_value = 0;

            return OK;
         }
      }

      /* Cache miss */

      /* Look for empty or available spaces */
      for (int i = 0; i < associativity; i++)
      {
         if (!cache_blocks[i].valid)
         {
            /* Search for the next empty position */
            pos_to_replace = i;
            break;
         }
         else if (cache_blocks[i].rp_value == distant_rp_value)
         {
            /* Search for the next available position */
            pos_to_replace = i;
            break;
         }
      }

      /* Case: no empty spaces */
      while (pos_to_replace == associativity)
      {
         /* Increase all rp values */
         for (int i = 0; i < associativity; i++)
         {
            cache_blocks[i].rp_value++;
         }
         /* Check if there is an available space */
         for (int i = 0; i < associativity; i++)
         {
            if (cache_blocks[i].rp_value == distant_rp_value)
            {
               pos_to_replace = i;
               break;
            }
         }
      }

      /* Check dirty bit */
      if (cache_blocks[pos_to_replace].dirty)
      {
         result->dirty_eviction = true;
         result->evicted_address = cache_blocks[pos_to_replace].tag; //tag viejo?
      }else
      {
         result->dirty_eviction = false;
      }

      /* Update entry information */
      cache_blocks[pos_to_replace].tag = tag;
      cache_blocks[pos_to_replace].rp_value = long_rp_value;
      cache_blocks[pos_to_replace].valid = true;
      cache_blocks[pos_to_replace].dirty = false;

      /* Update result information */
      result->miss_hit = MISS_LOAD;

      return OK;
   }
   /* Store case */
   else
   {
      /* Cache hit */
      for (int i = 0; i < associativity; i++)
      {
         if (cache_blocks[i].tag == tag && cache_blocks[i].valid)
         {
            result->miss_hit = HIT_STORE;
            result->dirty_eviction = false;

            /* Change rp_value */
            cache_blocks[i].rp_value = 0;

            /* Change dirty bit value */
            cache_blocks[i].dirty = true;

            return OK;
         }
      }

      /* Cache miss */

      /* Look for empty or available spaces */
      for (int i = 0; i < associativity; i++)
      {
         if (!cache_blocks[i].valid)
         {
            /* Search for the next empty position */
            pos_to_replace = i;
            break;
         }
         else if (cache_blocks[i].rp_value == distant_rp_value)
         {
            /* Search for the next available position */
            pos_to_replace = i;
            break;
         }
      }

      /* Case: no empty spaces */
      while (pos_to_replace == associativity)
      {
         /* Increase all rp values */
         for (int i = 0; i < associativity; i++)
         {
            cache_blocks[i].rp_value++;
         }
         /* Check if there is an available space */
         for (int i = 0; i < associativity; i++)
         {
            if (cache_blocks[i].rp_value == distant_rp_value)
            {
               pos_to_replace = i;
               break;
            }
         }
      }

      /* Check dirty bit */
      if (cache_blocks[pos_to_replace].dirty)
      {
         result->dirty_eviction = true;
         result->evicted_address = cache_blocks[pos_to_replace].tag;
      }else
      {
         result->dirty_eviction = false;
      }

      /* Update entry information */
      cache_blocks[pos_to_replace].tag = tag;
      cache_blocks[pos_to_replace].rp_value = long_rp_value;
      cache_blocks[pos_to_replace].valid = true;
      cache_blocks[pos_to_replace].dirty = true;

      /* Update result information */
      result->miss_hit = MISS_STORE;

      return OK;
   }

   return ERROR;

}


int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
  /* Negative values banned */
  if (idx < 0 || tag < 0) {
    return PARAM;
   }
  /* Flag for hit or miss */
  bool hit_or_miss = INITIAL_VALUE_empty;

  /* ------ CASE HIT ------ */
  for (int way = 0; way < associativity; way++) {
    /* Same tag and valid */
    if ((cache_blocks[way].tag == tag) && cache_blocks[way].valid) {

      hit_or_miss = HIT;
      /* ------ Load or Store ------ */
      if (loadstore == STORE) {
        cache_blocks[way].dirty = true;
        result->miss_hit = HIT_STORE;

      } else { /* loadstore == LOAD */
        result->miss_hit = HIT_LOAD;
      }
      /* There isn't eviction */
      result->dirty_eviction = false;

      /* Update rp_value */
      for (int way_rp_value = 0; way_rp_value < associativity; way_rp_value++) {
        if (cache_blocks[way_rp_value].rp_value > cache_blocks[way].rp_value){
           cache_blocks[way_rp_value].rp_value--;
         }
      }
      /* MRU */
      cache_blocks[way].rp_value = associativity - 1;

      /* break loop, hit found */
      break;
    }
  }
  /* ------ CASE MISS ------ */
  if (hit_or_miss == MISS) {

    for (int way = 0; way < associativity; way++) {
      /* LRU */
      if (cache_blocks[way].rp_value == LRU){
        /* ------ Eviction ------ */
        /*Dirty*/
        if (cache_blocks[way].dirty == true ) {
          result->dirty_eviction = true;

        } else {
          result->dirty_eviction = false;
        }
        /*Address*/
        result->evicted_address = cache_blocks[way].tag;

        /* ------ Load or Store ------ */
        if (loadstore == STORE) {
          cache_blocks[way].dirty = true;
          result->miss_hit = MISS_STORE;

        } else { /* loadstore == LOAD */
          cache_blocks[way].dirty = false;
          result->miss_hit = MISS_LOAD;
        }
        /* Update rp_value */
        for (int way_rp_value = 0; way_rp_value < associativity; way_rp_value++) {
           if (cache_blocks[way_rp_value].rp_value != LRU){
              cache_blocks[way_rp_value].rp_value--;
           }
        }
        /* MRU, update entry*/
        cache_blocks[way].rp_value = associativity - 1;
        cache_blocks[way].valid = true;
        cache_blocks[way].tag = tag;

        /* break loop, LRU found */
        break;
      }
    }
  }

   return OK;
}


int nru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
  /* Negative values banned */
  if (idx < 0 || tag < 0) {
    return PARAM;
   }
  /* Flag for hit or miss */
  bool hit_or_miss = INITIAL_VALUE_empty;

  /* ------ CASE HIT ------ */
  for (int way = 0; way < associativity; way++) {
    /* Same tag and valid */
    if ((cache_blocks[way].tag == tag) && cache_blocks[way].valid) {

      hit_or_miss = HIT;
      /* ------ Load or Store ------ */
      if (loadstore == STORE) {
        cache_blocks[way].dirty = true;
        result->miss_hit = HIT_STORE;

      } else { /* loadstore == LOAD */
        result->miss_hit = HIT_LOAD;
      }
      /* There isn't eviction */
      result->dirty_eviction = false;
      result->evicted_address = 0;

      /* NRU */
      cache_blocks[way].rp_value = NEAR;

      /* break loop, hit found */
      break;
    }
  }
  /* ------ CASE MISS ------ */
  if (hit_or_miss == MISS) {
    /* Flag for nru found */
    bool nru_found = false;

    /* Search nru distant */
    for (int way_rp_value = 0; way_rp_value < associativity; way_rp_value++) {
       if (cache_blocks[way_rp_value].rp_value == DISTANT){
          nru_found = true;
          break;
       }
    }
    /* Set all nru to distant */
    if (!nru_found) {
      for (int way_rp_value = 0; way_rp_value < associativity; way_rp_value++) {
          cache_blocks[way_rp_value].rp_value = DISTANT;
      }
    }

    for (int way = 0; way < associativity; way++) {
      /* NRU */
      if (cache_blocks[way].rp_value == DISTANT){
        /* ------ Eviction ------ */
        /*Dirty*/
        if (cache_blocks[way].dirty == true ) {
          result->dirty_eviction = true;
        } else {
          result->dirty_eviction = false;
        }
        /*Address*/
        result->evicted_address = cache_blocks[way].tag;

        /* ------ Load or Store ------ */
        if (loadstore == STORE) {
          cache_blocks[way].dirty = true;
          result->miss_hit = MISS_STORE;

        } else { /* loadstore == LOAD */
          cache_blocks[way].dirty = false;
          result->miss_hit = MISS_LOAD;
        }

        /* NEAR, update entry*/
        cache_blocks[way].rp_value = NEAR;
        cache_blocks[way].valid = true;
        cache_blocks[way].tag = tag;

        /* break loop, NRU found */
        break;
      }
    }
  }

   return OK;
}

entry **cache_matrix (int associativity,
                      int idx_size,
                      int rp)
{
  int ways = associativity;
  int sets = pow(2, idx_size);
  int M;
  /*Cache Matrix sets*/
  entry **cache = new entry*[sets];
  /*Cache Matrix ways*/
  for (int way = 0; way < sets; way++) {
  cache[way] = new entry[ways];
  }
  /*Cache initialization*/
  if (rp == LRU) {
  /* way[0] = 0 | way[1] = 1 | ... | way[n] = n */
    for (int set = 0; set < sets; set++) {
      for (int way = 0; way < ways; way++) {
        cache[set][way].valid = false;
        cache[set][way].dirty = false;
        cache[set][way].tag = 0;
        cache[set][way].rp_value = way;
      }
    }
  } else if (rp == NRU) {
  /* way[0] = 1 | way[1] = 1 | ... | way[n] = 1 */
    for (int set = 0; set < sets; set++) {
      for (int way = 0; way < ways; way++) {
        cache[set][way].valid = false;
        cache[set][way].dirty = false;
        cache[set][way].tag = 0;
        cache[set][way].rp_value = DISTANT;
      }
    }
  } else if (rp == RRIP) {
  /* way[0] = assoc-1 | way[1] = assoc-1 | ... | way[n] = assoc-1 */
    for (int set = 0; set < sets; set++) {
      for (int way = 0; way < ways; way++) {
        cache[set][way].valid = false;
        cache[set][way].dirty = false;
        cache[set][way].tag = 0;
        if (associativity > 2) {
          M = 2;
        } else {
          M = 1;
        }
        cache[set][way].rp_value = pow(2, M) - 1;
      }
    }
  }
  return cache;
}
